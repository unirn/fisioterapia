package br.edu.unirn.tipos

enum Semestre {
	
	PRIMEIRO_SEMESTRE("Primeiro Semestre"),SEGUNDO_SEMESTRE("Segundo Semestre")

	Date agora = new Date()	
	int ano = agora.year+1900
	
    String nome
	
	public Semestre(String nome){
		this.nome = nome
	}
	
	String toString(){
		nome+" - "+ano
	}

}
