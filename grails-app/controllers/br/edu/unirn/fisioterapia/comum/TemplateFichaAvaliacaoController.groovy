package br.edu.unirn.fisioterapia.comum

import org.springframework.dao.DataIntegrityViolationException;

import javassist.bytecode.stackmap.BasicBlock.Catch;
import br.edu.unirn.fisioterapia.CampoFichaAvaliacao
import br.edu.unirn.fisioterapia.TemplateFichaAvaliacao
import br.edu.unirn.tipos.fisioterapia.TipoCampo;

class TemplateFichaAvaliacaoController {

    def index() { 
		
		
	}
	def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [templateFichaAvaliacaoInstanceList: TemplateFichaAvaliacao.list(params), templateFichaAvaliacaoInstanceTotal: TemplateFichaAvaliacao.count()]
    }

    def create() {
        [templateFichaAvaliacaoInstance: new TemplateFichaAvaliacao(params)]
    }
	
	def show(Long id) {
		def templateFichaAvaliacaoInstance = TemplateFichaAvaliacao.get(id)
		if (!templateFichaAvaliacaoInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'templateFichaAvaliacao.label', default: 'Template'), id])
			redirect(action: "list")
			return
		}

		[templateFichaAvaliacaoInstance: templateFichaAvaliacaoInstance]
	}

	
	def save(){
		def templateFichaAvaliacaoInstance = new TemplateFichaAvaliacao(titulo:params.tituloTemplate)
		def msgError
		
		if(params.list('tituloCampo').size() != 1){
			params.tituloCampo.eachWithIndex {value, i ->
				def campo = new CampoFichaAvaliacao(titulo:value,tipoCampo:params.tipoCampo[i])
				if(campo){
					templateFichaAvaliacaoInstance.addToCampos(campo)
				}
			}
		}else{
			def campo = new CampoFichaAvaliacao(titulo:params.tituloCampo,tipoCampo:params.tipoCampo)
			if(campo){
				templateFichaAvaliacaoInstance.addToCampos(campo)
			}
		}
		
		if(!templateFichaAvaliacaoInstance.validate()){
			templateFichaAvaliacaoInstance.errors.allErrors.each {
				msgError += it.toString()
			}
			flash.message = msgError
			render(view: "create", model: [templateFichaAvaliacaoInstance: templateFichaAvaliacaoInstance])
			return
		}
		
		if (!templateFichaAvaliacaoInstance.save(flush: true)) {
			render(view: "create", model: [templateFichaAvaliacaoInstance: templateFichaAvaliacaoInstance])
			return
		}
		
		flash.message = message(code: 'default.created.message', args: [message(code: 'templateFichaAvaliacao.label', default: 'Template'), templateFichaAvaliacaoInstance.id])
		redirect(action: "show", id: templateFichaAvaliacaoInstance.id)
	}
	
	
	
	def edit(Long id) {
		def templateFichaAvaliacaoInstance = TemplateFichaAvaliacao.get(id)
		if (!templateFichaAvaliacaoInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'templateFichaAvaliacao.label', default: 'Template'), id])
			redirect(action: "list")
			return
		}
		[templateFichaAvaliacaoInstance: templateFichaAvaliacaoInstance]
	}

	def update(Long id, Long version) {
		def templateFichaAvaliacaoInstance = TemplateFichaAvaliacao.get(id)
		if (!templateFichaAvaliacaoInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'templateFichaAvaliacao.label', default: 'Template'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (templateFichaAvaliacaoInstance.version > version) {
				templateFichaAvaliacaoInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
						  [message(code: 'templateFichaAvaliacao.label', default: 'Template')] as Object[],
						  "Another user has updated this Template while you were editing")
				render(view: "edit", model: [templateFichaAvaliacaoInstance: templateFichaAvaliacaoInstance])
				return
			}
		}

		templateFichaAvaliacaoInstance.properties = params
		
		if (!templateFichaAvaliacaoInstance.save(flush: true)) {
			render(view: "edit", model: [templateFichaAvaliacaoInstance: templateFichaAvaliacaoInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'templateFichaAvaliacao.label', default: 'Template'), templateFichaAvaliacaoInstance.id])
		redirect(action: "show", id: templateFichaAvaliacaoInstance.id)
	}
	
	def delete(Long id) {
        def templateFichaAvaliacaoInstance = TemplateFichaAvaliacao.get(id)
        if (!templateFichaAvaliacaoInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'templateFichaAvaliacaoInstance.label', default: 'Template'), id])
            redirect(action: "list")
            return
        }

        try {
            templateFichaAvaliacaoInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'templateFichaAvaliacaoInstance.label', default: 'Template'), id])
            redirect(action: "list")
        }catch(DataIntegrityViolationException e){
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'templateFichaAvaliacaoInstance.label', default: 'Template'), id])
			redirect(action: "show", id: id)
        }
       
   }
}
