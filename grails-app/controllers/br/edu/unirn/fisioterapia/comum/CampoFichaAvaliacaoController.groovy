package br.edu.unirn.fisioterapia.comum

import org.springframework.dao.DataIntegrityViolationException

import br.edu.unirn.fisioterapia.CampoFichaAvaliacao

class CampoFichaAvaliacaoController {

    def index() { }
	
	
	def delete(Long id) {
		def campoFichaAvaliacaoInstace = CampoFichaAvaliacao.get(id)
		if (!campoFichaAvaliacaoInstace) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'campoFichaAvaliacaoInstace.label', default: 'Campo'), id])
			render (status: 500)
			return
		}

		try {
			campoFichaAvaliacaoInstace.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'campoFichaAvaliacaoInstace.label', default: 'Campo'), id])
			render (status: 200)
		}catch(DataIntegrityViolationException e){
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'campoFichaAvaliacaoInstace.label', default: 'Campo'), id])
			render (status: 500)
			}
		   
	   }
}
