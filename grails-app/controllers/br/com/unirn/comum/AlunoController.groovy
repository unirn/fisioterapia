package br.com.unirn.comum

import br.edu.unirn.fisioterapia.FichaAvaliacao;

class AlunoController {
	def alunoService
    
	def index() { }
	
	def listarAlunos(){
		render alunoService.listarAlunosVinculos()
	}
}
