<%@page import="br.edu.unirn.tipos.fisioterapia.TipoCampo"%>
<%@ page import="br.edu.unirn.fisioterapia.TemplateFichaAvaliacao" %>
<script type="text/javascript">
	
	
</script>


<div>
	<fieldset >
		<div class="control-group required">
			<label for="titulo">
				<g:message code="templateFichaAvaliacao.titulo.label" default="Titulo"/>
				<span class="required-indicador">*</span>
			</label>
			<div class="controls">
				<g:textField name="tituloTemplate" value="${templateFichaAvaliacaoInstance?.titulo}" />
			</div>
		</div>	
		<div class="control-group required">
			<label for="titulo">
				<g:message code="campoFichaAvaliacao.titulo.label" default="Campo" />
				<span class="required-indicador">*</span>
			</label>
			<div class="controls">
				<g:textField name="tituloCampoValor" value=""/>
				<g:select name="tipoCampoValor" from="${TipoCampo.values()}" keys="${TipoCampo.values()*.name()}" value=""/>
				<a href="javascript:void(0)" class="btn btn-mini btn-info" style="margin-bottom: 10px;" onclick="addListaCampo()" ><i class="icon-plus-sign"></i></a>
			</div>
		</div>
	</fieldset>
	<div class="control-group tipoCampos">
		<div class="controls">
			<div class="span3">
				<table class="table table-bordered table-condensed">
					<thead>
						<tr>
							<th>Campo</th>
							<th>Tipo</th>
							<th></th>
						</tr>
					</thead>
					<tbody id='campos'>
					<g:each var="campo" in="${templateFichaAvaliacaoInstance.campos}">
						<tr id="${campo.id}">
							<input type="hidden" name="tituloCampo" value="${campo.tipoCampo}">
							<input type="hidden" name="tipoCampo" value="${campo.tipoCampo}">
							<td>${campo.titulo}</td>
							<td>${campo.tipoCampo}</td>
							<td><a href="javascript:void(0)" class="btn btn-mini btn-danger ${actionName.equals("edit")?"":removerCampo}" ${actionName.equals("edit")?"onclick='removerCampo(${campo.id})'":""}><i class="icon-trash"></i></a></td>
						</tr>
					</g:each>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

