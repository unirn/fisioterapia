<!doctype html>
<html lang="en">
<head>
<title><g:layoutTitle
		default="Clínicas Integradas - Núcleo DEV UNIRN" /></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<r:require module="jquery-ui" />
<r:require module="bootstrap" />
<g:layoutHead />
<r:layoutResources />
<g:javascript src="jquery.maskedinput-1.3.js" />
<link rel="stylesheet" href="${resource(dir: 'css', file: 'style.css')}" type="text/css">
<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
}

.sidebar-nav {
	padding: 9px 0;
}

input:invalid,select:invalid,textarea:invalid {
	background: none repeat scroll 0 0 #FFF3F3;
	border-color: #FFAAAA;
	color: #CC0000;
}

.required-indicator {
	color: #48802C;
	display: inline-block;
	font-weight: bold;
	margin-left: 0.3em;
	position: relative;
	top: 0.1em;
}
</style>
</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a href="${createLink(uri: '/')}" class="brand">Fisioterapia</a>
				<div class="nav-collapse collapse">
					<ul class="nav">
						<li><g:link controller="paciente" action="create">Paciente</g:link></li>
						<li><g:link controller="consulta" action="create">Consulta</g:link></li>
						<li><g:link controller="buscaConsulta">Agenda</g:link></li>
					</ul>
					<ul class="nav pull-right">
						<li><a href="#" id="linkAbrirChamado">Sugestão</a></li>
						<li><g:link controller="logout">Sair:
							</g:link></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span3">
				<div class="well sidebar-nav">
					<ul class="nav nav-list">
						<li class="nav-header">Paciente</li>
						<li><g:link controller="paciente">Lista</g:link></li>
						<li><g:link controller="buscaPaciente">Buscar</g:link></li>
						<li class="nav-header">Consulta</li>
						<li><g:link controller="buscaConsulta">Agenda</g:link></li>
						<li class="nav-header">Avaliação</li>
						<li><g:link controller="buscaConsulta">Listar Avaliações</g:link></li>
						<li class="nav-header">Evolução</li>
						<li><g:link controller="buscaConsulta">Cadastrar Evolução</g:link></li>
						<li class="nav-header">Evolução Avulsa</li>
						<li><g:link controller="buscaConsulta">Cadastrar Evolução Avulsa</g:link></li>
					</ul>
				</div>
			</div>

			<div class="span9">
				<div class="row-fluid">
					<g:layoutBody />
				</div>
			</div>
		</div>
	</div>

	<div class="footer" role="contentinfo"></div>
	<div id="spinner" class="spinner" style="display: none;">
		<g:message code="spinner.alt" default="Loading&hellip;" />
	</div>
	<g:javascript library="application" />
	<r:layoutResources />
	<div id="abrirChamado" title="Abertura de Sugestão">
		<g:formRemote method="post" name="frmChamado"
			url="[controller:'chamado',action:'aberturaChamado']"
			onComplete="resetForm('#frmChamado');closeDialog('#abrirChamado');refreshPage();">
			<p>
				<label for="titulo" class="control-label"> Título <span
					class="required-indicator"></span>
				</label>
				<g:textField class="input-xlarge" name="titulo"
					placeholder="Título da Sugestão" />
			</p>
			<p>
				<label for="descricao" class="control-label"> Descrição </label>
				<g:textArea name="mensagem" rows="6" style="width: 100%;" />
			</p>
			<p>
				<g:actionSubmit class="btn" value="Enviar Sugestão" />
			</p>
		</g:formRemote>
	</div>
</body>
</html>