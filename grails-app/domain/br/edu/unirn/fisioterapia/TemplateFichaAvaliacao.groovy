package br.edu.unirn.fisioterapia

class TemplateFichaAvaliacao {
	String titulo
	static hasMany = [campos:CampoFichaAvaliacao]
	
    static constraints = {
		titulo(nullable:true)
    }
}
