package br.edu.unirn.fisioterapia

import br.edu.unirn.tipos.fisioterapia.TipoCampo

class CampoFichaAvaliacao {
	String titulo
	TipoCampo tipoCampo
	static belongsTo = [template:TemplateFichaAvaliacao]
	
    static constraints = {
		titulo(nullable:true)
		tipoCampo(nullable:true)
		
    }
}
