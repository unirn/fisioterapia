package br.edu.unirn.fisioterapia

import br.edu.unirn.comum.Profissional

class Evolucao {

	Boolean avulso
	Boolean aceitoAvulso
	Profissional lancamento
	Profissional lancamentoAvulso
	
    static constraints = {
		avulso(nullable:true) 
		aceitoAvulso(nullable:true)
		lancamentoAvulso(nullable:true)
    }
}
