package br.edu.unirn.comum

class Sala {

	String nome
	Clinica clinica
	
    static constraints = {
		nome(nullable:false,blank:false,unique:true)
		clinica(nullable:false)
    }
	
	static mapping = {
		sort('nome')
	}
	
	String toString(){
		nome	
	}
}
