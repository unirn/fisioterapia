package br.edu.unirn.comum


class Paciente {
	
	Pessoa pessoa
	Clinica encaminhado
	//Usuario usuarioCadastro
	boolean inativo
	
    static constraints = {
		encaminhado(nullable:true)
    }
	
	static mapping = {
		pessoa cascade:"all"
	}
	
	String toString(){
		pessoa.nome	
	}
}
