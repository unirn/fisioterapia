package br.edu.unirn.comum

import br.edu.unirn.tipos.Sexo
import br.edu.unirn.utils.StringUtils


class Pessoa {

	String nome
	String nomeAscii
	String nomePai
	String nomeMae
	String email
	Sexo sexo
	Date dataNascimento
	String observacao
	
	//Contato
	String telefoneContato
	String telefoneCelular
		
    static constraints = {
		nome(nullable:false,blank:false,size:0..200)
		nomePai(nullable:true,size:0..200)
		nomeMae(nullable:true,blank:true,size:0..200)
		email(nullable:true,email:true)
		sexo(nullable:false)
		dataNascimento(nullable:false)
		telefoneContato(blank:false,nullable:false)
		observacao(blank:true,nullable:true)
		nomeAscii(blank:true,nullable:true)
    }
	
	static mapping = {
		nomeAscii(index:'idx_nomeAscii_funcionario')
		sort "nomeAscii"
	}
	
	String toString(){
		nome
	}
	
		
	def beforeInsert() {
		nomeAscii = StringUtils.toAscii(nome)
	}

	def beforeUpdate() {
		if (isDirty('nome')) {
			nomeAscii = StringUtils.toAscii(nome)
		}
	}
}
