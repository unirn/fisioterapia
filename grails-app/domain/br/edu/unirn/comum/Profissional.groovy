package br.edu.unirn.comum

import br.edu.unirn.tipos.Semestre

class Profissional {

	Pessoa pessoa
	Papel papel
	
	Clinica clinica			
	
	Semestre semestre
    
	static constraints = {
		papel(nullable:false)
    	clinica(nullable:true)
		semestre(nullable:true)
	}
	
	static mapping = {
		pessoa cascade:"all"
	}
	
	String toString(){
		pessoa.nome	
	}
}
