package br.edu.unirn.comum

class Papel {
	
	Integer id
	String nome

	static mapping = {
		cache(usage:'read-only')
		version false
		id(generator:'assigned')
	}
	
	String toString(){
		nome
	}
}
