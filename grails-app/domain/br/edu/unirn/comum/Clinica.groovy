package br.edu.unirn.comum

class Clinica {
	
	String descricao
	Float valor

    static constraints = {
		descricao(nullable:false,unique:true)
		valor(nullable:false,blank:false)
    }
	
	static mapping = {
		sort('descricao')
	}
	
	String toString(){
		descricao
	}
	
}
