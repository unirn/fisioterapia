package br.edu.unirn.consulta

import br.edu.unirn.comum.Paciente
import br.edu.unirn.comum.Sala
import br.edu.unirn.tipos.Turno


class Consulta {

	Paciente paciente
	Date dataConsulta
	String horarioConsulta
	Sala sala
	Sala salaIndicacao
	String motivo
	String justificativa
	StatusConsulta statusConsulta
	Turno turno
	
	//Usuario usuarioMarcacao
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
		statusConsulta(nullable:true)
    }
	
}
