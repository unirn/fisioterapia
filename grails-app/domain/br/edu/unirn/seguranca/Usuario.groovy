package br.edu.unirn.seguranca

import br.edu.unirn.comum.Pessoa;

class Usuario {

	String nome
	String email
	String login
	String senha
	boolean habilitado = true
	
	static constraints = {
		login blank: false, unique: true
		senha blank: false
		nome blank:false
		email email:true, blank:false
	}
		
	String toString(){
		nome
	}
}
