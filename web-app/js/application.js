if (typeof jQuery !== 'undefined') {
	(function($) {
		$('#spinner').ajaxStart(function() {
			$(this).fadeIn();
		}).ajaxStop(function() {
			$(this).fadeOut();
		});
	})(jQuery);
}

$(document).ready(function() {
	$('[name*=telefone]').mask('(99) 9999-9999')
	$('[name*=cep]').mask('99999-999')
	$('[name^=hora]').mask('99:99')
	$('input[name^=data][type=text]').mask('99/99/9999')

	$('#abrirChamado').dialog({
		autoOpen : false,
		height : 370,
		width : 450,
		modal : true
	});

	$("#linkAbrirChamado").click(function() {
		$("#abrirChamado").dialog("open");
	});
	
	$(document).on("click",".removerCampo",function(){
		$(this).parent().parent().remove()
	})
});

function closeDialog(idDialog) {
	$(idDialog).dialog("close");
}

function resetForm(idForm) {
	$(idForm).each(function() {
		this.reset();
	});
}

function refreshPage() {
	location.reload();
}

function addListaCampo(){
	var campo = $("input[name='tituloCampoValor']").val()
	var tipo = $("select option:selected").val()
	$("tbody#campos").append('<tr><input type="hidden" name="tituloCampo" value="'+campo+'"><input type="hidden" name="tipoCampo" value="'+tipo+'"><td>'+campo+'</td><td>'+tipo+'</td><td><a href="javascript:void(0)" class="btn btn-mini btn-danger removerCampo"><i class="icon-trash"></i></a></td></tr>')
	
	$("input[name='tituloCampoValor']").val('')
}

function  removerCampo(id){
	console.info(id)
	$.ajax({
		url:"/fisioterapia/campoFichaAvaliacao/delete/"+id,
		type:"DELETE",
		success:function(){
			$("#"+id).remove()
		}
	})
}
